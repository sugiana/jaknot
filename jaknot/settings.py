# Scrapy settings for jaknot project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'jaknot'
SPIDER_MODULES = ['jaknot.spiders']
NEWSPIDER_MODULE = 'jaknot.spiders'
