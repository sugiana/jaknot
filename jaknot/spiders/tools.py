import os
import re
import string
from urlparse import urlsplit
from scrapy import (
    Request,
    FormRequest,
    )
from scrapy.selector import Selector
from scrapy.spiders import Spider
from scrapy.exceptions import CloseSpider
from bs4 import BeautifulSoup


def v(values):
    s = ' '.join(values)
    return s.strip()
    
def one_space(s):
    while s.find('  ') > -1:
        s = s.replace('  ', ' ')
    return s
    
def clean_char(ch):
    if ch == u'\u201d':
        return '"'
    if ch == u'\xd7':
        return 'x'
    return ch in string.printable and ch or ' '        
    
def clean(s):
    s = ''.join([clean_char(ch) for ch in s])
    s = one_space(s)
    if s.find('<span') < 0 and s.find('<br') < 0:
        return s
    soup = BeautifulSoup(s, 'lxml')
    for link in soup.find_all('span'):
        link.unwrap()
    s = str(soup).replace('<br/>', '\n').\
            lstrip('<html><body><p>').\
            rstrip('</p></body></html>')
    return s    
 
def get_image(sel, url):
    images = sel.xpath('//meta[@property="og:image"]/@content')
    for s in images:
        return s.extract()

def file_as_list(filename):
    if not os.path.exists(filename):
        return []
    s_list = []
    f = open(filename)
    for line in f.readlines():
        s = line.strip()
        s_list.append(s)
    f.close()
    return s_list

def is_url_local(url):
    return url.find('file://') > -1

def url2file(url):
    return url.replace('file://','').rstrip('/')
    
def is_url_local_file(url):
    if not is_url_local(url):
        return
    filename = url2file(url)
    return os.path.isfile(filename)

def get_start_urls(url):
    if not is_url_local(url):
        return [url]
    if is_url_local_file(url):
        return [url]
    dir_name = url2file(url)
    r = []
    for filename in os.listdir(dir_name):
        url_local = 'file://' + os.path.join(dir_name, filename)
        r.append(url_local)
    return r    
    
def parse_url(sel):
    s = sel.xpath('//meta[@property="og:url"]').xpath('@content').extract()
    return v(s)
    
def parse_title(sel):    
    s = sel.xpath('//div[@class="title"]/span/h1/text()').extract()
    return v(s)
    
def parse_description(sel):
    for tag in ('p', 'blockquote', 'table/tbody/tr/td/p'):
        tpl = '//div[@class="boxContent mce"]/{t}/text()'.format(t=tag)
        for s in sel.xpath(tpl).extract():
            r = clean(s)
            if r:
                return r
    
def parse_price(sel):
    s = v(sel.xpath('//span[@itemprop="price"]/text()').extract())
    val = s.lower().replace('.', '')
    if not val:
        return
    currency = 'idr'
    amount = int(val)
    return [s, amount, currency]
    
def parse_brand(sel):
    title = parse_title(sel)
    return title.split()[0]

def parse_model(sel):
    s = sel.xpath('//meta[@itemprop="model"]').xpath('@content').extract()
    return v(s)


XPATH_SPECS = '//section[@class="detailSection techSpec"]/'\
              'div[@class="box detailBox"]/div[@class="boxContent"]/table/tr'

class CommonSpider(Spider):
    allowed_domains = ['jakartanotebook.com']
    product_class = None # override please
    blacklist_brand_file = None
    blacklist_urls = (
        'https://www.jakartanotebook.com/terms',
        'https://www.jakartanotebook.com/contact',
        )
    
    def __init__(self, product_url=None, save_dir=None, *args, **kwargs):
        super(CommonSpider, self).__init__(*args, **kwargs)
        self.product_url = product_url
        self.save_dir = save_dir
        if product_url:
            self.start_urls = get_start_urls(product_url)
        self.blacklist_brand = self.blacklist_brand_file and \
            file_as_list(self.blacklist_brand_file) or []
        self.next_url = []

    # Override from parent class
    def parse(self, response):
        if self.product_url:
            yield self.parse_product(response)
            return    
        xs = Selector(response)
        for s in xs.xpath('//a[@class="product-list__title"]/@href'):
            url = s.extract()
            yield self.product_request(response, url)
        for s in xs.xpath('//a[@title="Next"]/@href'):
            url = s.extract()
            if url in self.next_url:
                continue
            self.next_url.append(url)
            yield self.next_page_request(response, url)

    def next_page_request(self, response, url):
        return FormRequest(url=url, callback=self.parse)

    def product_request(self, response, url):
        return FormRequest(url=url, callback=self.parse_product_)

    def product_instance(self, response):
        return self.product_class()
        
    def parse_product_(self, response):
        if response.url not in self.blacklist_urls:
            yield self.parse_product(response)        

    def parse_product(self, response):
        if self.save_dir:
            self.save(response)    
        sel = Selector(response)
        i = self.product_instance(response)        
        i = self.parse_brand(response, sel, i)        
        if self.is_blacklist(response, i):
            return        
        i = self.parse_url(response, sel, i)
        i = self.parse_title(response, sel, i)
        i = self.parse_description(response, sel, i)
        i = self.parse_picture(response, sel, i)
        i = self.parse_price(response, sel, i)
        i = self.parse_model(response, sel, i)
        specs = sel.xpath(XPATH_SPECS)
        for spec in specs:
            cols = spec.xpath('td/text()')
            key = cols[0].extract()
            cols = spec.xpath('td')
            val = cols[1].extract()
            val = val[4:-5] # buang <td></td>
            vals = []
            for v in val.split('<br>'):
                vals.append(v.strip())
            for spec_name in self.specs:
                spec_vals = self.specs[spec_name]
                if key in spec_vals:
                    i[spec_name] = vals
        return i
    
    def parse_url(self, response, sel, i):
        s = parse_url(sel)
        if s:
            i['url'] = s
        return i

    def parse_title(self, response, sel, i):
        s = parse_title(sel)
        if s:
            i['title'] = s
        return i
        
    def parse_description(self, response, sel, i):
        s = parse_description(sel)
        if s:
            i['description'] = s
        else:
            self.log_missing(response, 'description')
        return i
        
    def parse_picture(self, response, sel, i):
        image = get_image(sel, response.url)
        if image:
            i['picture'] = image
        return i
                    
    def parse_price(self, response, sel, i):
        price = parse_price(sel)
        if price:
            i['price'] = price
        return i
        
    def parse_brand(self, response, sel, i):
        brand = parse_brand(sel)
        if brand:
            i['brand'] = brand
        return i
        
    def parse_model(self, response, sel, i):
        model = parse_model(sel)
        if model:
            i['model'] = model
        return i 
        
    def save(self, response):
        u = urlsplit(response.url)
        filename = os.path.split(u.path)[-1]
        fullpath = os.path.join(self.save_dir, filename)
        f = open(fullpath, 'w')
        f.write(response.body)
        f.close()        
        
    def is_blacklist(self, response, data):
        if 'brand' in data:
            b = data['brand'].strip().lower()
            if b in self.blacklist_brand:
                self.logger.info('{url} brand {b} di-blacklist.'.\
                    format(url=response.url, b=b))
                return True

    def log_missing(self, response, key):
        self.logger.warning('Tidak ada {k} di {u}'.format(k=key, u=response.url))
                        
        
######################
# Regular Expression #
######################
def regex_compile(regexs):
    r = []
    for regex in regexs:
        c = re.compile(regex)
        r.append(c)
    return r

def regex_units_compile(templates):
    r = []
    for tpl in templates:
        jml = tpl.count('%s')
        ru = tuple()
        for i in range(jml):
            ru += (REGEX_UNITS,)
        s = tpl % ru 
        c = re.compile(s)
        r.append(c)
    return r
    
def c_regex_search(c_regex, text):
    return c_regex.search(text.lower())

# Saat ambil nilai dari sumber teks bebas seperti description maka salin
# teks yang dimaksud seperlunya saja, jangan keseluruhan description.    
def regex_get_original_words(pattern, str_lower, str_orig):
    pattern = '({s})'.format(s=pattern)
    match = re.compile(pattern).search(str_lower)
    found = match.group(1)
    p = str_lower.find(found)
    return str_orig[p:p+len(found)]
    
##########
# Memory #
##########
UNITS = ['mb', 'gb', 'tb']
REGEX_UNITS = '(%s)' % '|'.join(UNITS)    

###########
# Numeric #
###########    
def should_int(value):
    int_ = int(value)
    return int_ == value and int_ or value
